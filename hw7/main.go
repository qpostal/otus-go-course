package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"gitlab.com/aamoiseev/otus-go-course/hw7/goenv"
)

func main() {
	if len(os.Args) < 3 {
		printUsage()
	}

	dir := os.Args[1]
	child := os.Args[2]
	args := os.Args[3:]

	dirEnv := &goenv.DirEnv{
		ReadDir:  ioutil.ReadDir,
		ReadFile: ioutil.ReadFile,
	}

	env := &goenv.Env{
		Unsetenv: os.Unsetenv,
		Setenv:   os.Setenv,
	}

	execCmd := &goenv.Cmd{
		Command: func(name string, arg ...string) goenv.Runner {
			cmd := exec.Command(name, arg...)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			return cmd
		},
	}

	cmd := &Cmd{
		dirEnv,
		env,
		execCmd,
	}

	err := cmd.Run(dir, child, args...)
	if err != nil {
		writeError("command error: %v", err)
	}
}

func printUsage() {
	usage := fmt.Sprintf(
		`usage: %s d child
	sets various environment variables as specified by files in the directory named  d. It then runs child.
		`,
		os.Args[0],
	)

	writeError(usage, nil)
}

func writeError(format string, err error) {
	if err != nil {
		format = fmt.Sprintf(format, err)
	}

	_, _ = fmt.Fprintln(os.Stderr, format)
	os.Exit(1)
}
