package goenv

import (
	"fmt"
	"os"
	"strings"
)

type EnvReader interface {
	Read(dirname string) (map[string]string, error)
}

type DirEnv struct {
	ReadDir  func(dirname string) ([]os.FileInfo, error)
	ReadFile func(filename string) ([]byte, error)
}

func (de *DirEnv) Read(dirname string) (map[string]string, error) {
	dirname = strings.TrimRight(dirname, string(os.PathSeparator))

	files, err := de.ReadDir(dirname)
	if err != nil {
		return nil, err
	}

	env := make(map[string]string)

	for _, f := range files {
		if !f.IsDir() {
			envName := f.Name()
			envValue, err := de.ReadFile(dirname + string(os.PathSeparator) + envName)
			if err != nil {
				return nil, err
			}

			if strings.Contains(envName, "=") {
				return nil, fmt.Errorf(`name should not contain = (in %s)`, envName)
			}

			env[envName] = strings.TrimSpace(string(envValue))
		}
	}

	return env, nil
}
