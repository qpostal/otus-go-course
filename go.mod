module gitlab.com/aamoiseev/otus-go-course

go 1.13

require (
	github.com/beevik/ntp v0.2.0
	github.com/cheggaaa/pb/v3 v3.0.1
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
	golang.org/x/net v0.0.0-20190912160710-24e19bdeb0f2 // indirect
)
