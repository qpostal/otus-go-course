package unpack

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDo(t *testing.T) {
	tests := [][]string{
		{"", ""},
		{"a", "a"},
		{"a1", "a"},
		{"a0", ""},
		{"a0b0", ""},
		{"a0b", "b"},
		{"ab0", "a"},
		{"a0b0c2d0", "cc"},
		{"a11bc12de14", "aaaaaaaaaaabccccccccccccdeeeeeeeeeeeeee"},
		{"a111b", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"},
		{"a4bc2d5e", "aaaabccddddde"},
		{"abcd", "abcd"},
		{`qwe\4\5`, "qwe45"},
		{`qwe\45`, "qwe44444"},
		{`qwe\\5`, `qwe\\\\\`},
		{`\\`, `\`},
		{`a01`, `a`},
		{`a001`, `a`},
		{"世2界3", "世世界界界"},
	}

	for _, test := range tests {
		input, expected := test[0], test[1]

		actual, err := Do(input)
		if err != nil {
			t.Fatalf(`unexpected error: %s on "%s" input`, err, input)
		}

		assert.Equal(t, expected, actual)
	}
}

func TestDoError(t *testing.T) {
	tests := [][]string{
		{`\`, `unexpected \ at position 1`},
		{`\a`, `unexpected \ at position 1`},
		{`\\\`, `unexpected \ at position 3`},
		{"4", "unexpected 4 at position 1"},
		{"45a", "unexpected 4 at position 1"},
	}

	for _, test := range tests {
		input, inputErr := test[0], test[1]

		_, err := Do(input)
		if err == nil {
			t.Fatalf(`expected error for input: %v`, input)
		}

		assert.EqualError(t, err, inputErr)
	}
}
