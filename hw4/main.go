package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/aamoiseev/otus-go-course/hw4/collections"
)

func main() {
	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "stdin read: %v", err)
		os.Exit(1)
	}

	list := collections.NewList()

	words := strings.Fields(string(bytes))
	for _, word := range words {
		list.PushBack(word)
	}

	list.Reverse()

	list.Each(func(item *collections.Item) {
		fmt.Print(item.Value())

		if item.Next() != nil {
			fmt.Print(" ")
		}
	})

	fmt.Println()
}
