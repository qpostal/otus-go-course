package words

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTop(t *testing.T) {
	tests := []string{
		"   one two\n two! three. three\t three ...",
		"One, ONe, OnE, one, onE, oNe! two two",
	}

	testResults := [][]string{
		{
			"three",
			"two",
		},
		{
			"one",
			"two",
		},
	}

	for i, text := range tests {
		actual := Top(text, len(testResults[i]))

		assert.Equal(t, testResults[i], actual)
	}
}

func TestTopWithEmptyText(t *testing.T) {
	result := Top("", 10)

	assert.Equal(t, make([]string, 0), result)
}
