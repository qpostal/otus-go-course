package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"gitlab.com/aamoiseev/otus-go-course/hw3/words"
)

const (
	defaultCount = 10
)

func main() {
	n := defaultCount

	if len(os.Args) == 2 {
		num, err := strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\nUsage: %s [count]\n", err, os.Args[0])
			os.Exit(1)
		}
		n = num
	}

	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "stdin read: %v", err)
		os.Exit(1)
	}

	freqWords := words.Top(string(bytes), n)
	for _, word := range freqWords {
		fmt.Println(word)
	}
}
